BINDIR=bin

.PHONY: all
all: $(BINDIR)/unisense

$(BINDIR)/unisense: $(shell find ./ -type f -name '*.go') | $(BINDIR)
	GOARCH=arm go build -o $@

$(BINDIR):
	mkdir -p $@

.PHONY: clean
clean:
	$(RM) -r $(BINDIR)

deploy: $(BINDIR)/unisense
	scp $< pi@192.168.11.140:/tmp
