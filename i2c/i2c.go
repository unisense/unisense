package i2c

import (
	"fmt"
	"os"
	"syscall"
	"unsafe"
)

const I2C_FUNCS = uintptr(0x0705)
const I2C_FUNC_I2C = 0x00000001
const I2C_M_RD = 0x0001
const I2C_RDWR = uintptr(0x0707)

type I2C struct {
	f       *os.File
	address uint8
}

func NewI2C(path string, address uint8) *I2C {
	i := &I2C{}
	f, err := os.OpenFile(path, syscall.O_RDWR, 0666)
	if err != nil {
		fmt.Fprintf(os.Stderr, "cannot open file\n")
		return nil
	}
	i.f = f
	var flags [1]uintptr
	_, _, e := syscall.Syscall(syscall.SYS_IOCTL, f.Fd(), I2C_FUNCS, uintptr(unsafe.Pointer(&flags[0])))
	if e != 0 {
		f.Close()
		fmt.Fprintf(os.Stderr, "cannot check i2c functionality\n")
		return nil
	}
	if flags[0]&I2C_FUNC_I2C == 0 {
		f.Close()
		fmt.Fprintf(os.Stderr, "not supported i2c functionality\n")
		return nil
	}
	i.address = address
	return i
}

func (i *I2C) Close() error {
	return i.f.Close()
}

type i2c_msg struct {
	addr  uint16
	flags uint16
	len   uint16
	buf   unsafe.Pointer
}

type i2c_rdwr_ioctl_data struct {
	msgs  unsafe.Pointer
	nmsgs uint32
}

func (i *I2C) Read(register_address uint8, buf []uint8, buf_size uint) (uint, error) {
	if i == nil {
		return 0, fmt.Errorf("receiver is nil")
	}
	buf1 := make([]uint8, 1)
	buf1[0] = register_address
	messages := []i2c_msg{
		{
			addr:  uint16(i.address),
			flags: 0,
			len:   uint16(len(buf1)),
			buf:   unsafe.Pointer(&buf1[0]),
		}, {
			addr:  uint16(i.address),
			flags: I2C_M_RD,
			len:   uint16(buf_size),
			buf:   unsafe.Pointer(&buf[0]),
		},
	}
	message_pack := i2c_rdwr_ioctl_data{
		msgs:  unsafe.Pointer(&messages[0]),
		nmsgs: uint32(len(messages)),
	}
	_, _, e := syscall.Syscall(syscall.SYS_IOCTL, i.f.Fd(), I2C_RDWR, uintptr(unsafe.Pointer(&message_pack)))
	if e != 0 {
		return 0, fmt.Errorf("cannot write i2c")
	}
	return buf_size, nil
}

func (i *I2C) Write(register_address uint8, buf []uint8, buf_size uint) (uint, error) {
	if i == nil {
		return 0, fmt.Errorf("receiver is nil")
	}
	buf1 := make([]uint8, 1)
	buf1[0] = register_address
	buf1 = append(buf1, buf...)
	messages := []i2c_msg{
		{
			addr:  uint16(i.address),
			flags: 0,
			len:   uint16(len(buf1)),
			buf:   unsafe.Pointer(&buf1[0]),
		},
	}
	message_pack := i2c_rdwr_ioctl_data{
		msgs:  unsafe.Pointer(&messages[0]),
		nmsgs: uint32(len(messages)),
	}
	_, _, e := syscall.Syscall(syscall.SYS_IOCTL, i.f.Fd(), I2C_RDWR, uintptr(unsafe.Pointer(&message_pack)))
	if e != 0 {
		return 0, fmt.Errorf("cannot write i2c")
	}
	return buf_size, nil
}
