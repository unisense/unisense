package i2c

type I2CConn interface {
	Read(register_address uint8, buf []uint8, buf_size uint) (uint, error)
	Write(register_address uint8, buf []uint8, buf_size uint) (uint, error)
	Close() error
}

func Setregister(conn I2CConn, address uint8, value uint8, mask uint8) error {
	data_out := make([]uint8, 1)
	_, err := conn.Read(address, data_out, 1)
	if err != nil {
		return err
	}
	data_in := data_out[0]&mask | value
	_, err = conn.Write(address, []uint8{data_in}, 1)
	if err != nil {
		return err
	}
	return nil
}

func ReadOneByte(conn I2CConn, register_address uint8, v *uint8) (uint, error) {
	buf := make([]uint8, 1)
	_, err := conn.Read(register_address, buf, 1)
	if err != nil {
		return 0, err
	}
	*v = buf[0]
	return 2, nil
}

func ReadTwoBytes(conn I2CConn, register_address uint8, v *uint16) (uint, error) {
	buf := make([]uint8, 2)
	_, err := conn.Read(register_address|0x80, buf, 2)
	if err != nil {
		return 0, err
	}
	*v = uint16(buf[1])
	*v <<= 8
	*v |= uint16(buf[0])
	return 2, nil
}
