package hts221

import (
	"fmt"

	"gitlab.com/unisense/unisense/i2c"
)

type HTS221 struct {
	conn i2c.I2CConn
}

func New(path string) *HTS221 {
	s := &HTS221{}
	conn := i2c.NewI2C(path, 0x5F)
	if conn == nil {
		return nil
	}
	s.conn = conn
	return s
}

func (s *HTS221) Close() error {
	return s.conn.Close()
}

type TemperatureSampleAveraging uint8

const (
	TemperatureSampleAveraging2   TemperatureSampleAveraging = iota
	TemperatureSampleAveraging4                              = iota * 8
	TemperatureSampleAveraging8                              = iota * 8
	TemperatureSampleAveraging16                             = iota * 8
	TemperatureSampleAveraging32                             = iota * 8
	TemperatureSampleAveraging64                             = iota * 8
	TemperatureSampleAveraging128                            = iota * 8
	TemperatureSampleAveraging256                            = iota * 8
)

func (s *HTS221) WithTemperatureSampleAveraging(v TemperatureSampleAveraging) *HTS221 {
	err := i2c.Setregister(s.conn, 0x10, uint8(v), 0b1100_0111)
	if err != nil {
		panic(err)
	}
	return s
}

type HumiditySampleAveraging uint8

const (
	HumiditySampleAveraging4 HumiditySampleAveraging = iota
	HumiditySampleAveraging8
	HumiditySampleAveraging16
	HumiditySampleAveraging32
	HumiditySampleAveraging64
	HumiditySampleAveraging128
	HumiditySampleAveraging256
	HumiditySampleAveraging512
)

func (s *HTS221) WithHumiditySampleAveraging(v HumiditySampleAveraging) *HTS221 {
	err := i2c.Setregister(s.conn, 0x10, uint8(v), 0b1111_1000)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) Enable() *HTS221 {
	err := i2c.Setregister(s.conn, 0x20, 0x80, 0b0111_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) Disable() *HTS221 {
	err := i2c.Setregister(s.conn, 0x20, 0x00, 0b0111_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithContinousUpdate() *HTS221 {
	err := i2c.Setregister(s.conn, 0x20, 0x00, 0b1111_1011)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithTriggeredUpdate() *HTS221 {
	err := i2c.Setregister(s.conn, 0x20, 0x04, 0b1111_1011)
	if err != nil {
		panic(err)
	}
	return s
}

const (
	hts221_datarate_oneshot uint8 = iota
	hts221_datarate_1hz
	hts221_datarate_7hz
	hts221_datarate_125hz
)

func (s *HTS221) withDataRate(dr uint8) *HTS221 {
	err := i2c.Setregister(s.conn, 0x20, dr, 0b1111_1100)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataRateOneShot() *HTS221 {
	return s.withDataRate(hts221_datarate_oneshot)
}

func (s *HTS221) WithDataRate1Hz() *HTS221 {
	return s.withDataRate(hts221_datarate_1hz)
}

func (s *HTS221) WithDataRate7Hz() *HTS221 {
	return s.withDataRate(hts221_datarate_7hz)
}

func (s *HTS221) WithDataRate12dot5Hz() *HTS221 {
	return s.withDataRate(hts221_datarate_125hz)
}

func (s *HTS221) Reboot() *HTS221 {
	err := i2c.Setregister(s.conn, 0x21, 0x80, 0b0111_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) EnableHeater() *HTS221 {
	err := i2c.Setregister(s.conn, 0x21, 0x02, 0b1111_1101)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) DisableHeater() *HTS221 {
	err := i2c.Setregister(s.conn, 0x21, 0x00, 0b1111_1101)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) MeasureOneShot() *HTS221 {
	err := i2c.Setregister(s.conn, 0x21, 0x01, 0b1111_1110)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyOutputSignalActiveHigh() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x00, 0b0111_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyOutputSignalActiveLow() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x80, 0b0111_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyOutputPushPull() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x00, 0b1011_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyOutputOpenDrain() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x40, 0b1011_1111)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyEnabled() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x04, 0b1111_1011)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) WithDataReadyDisabled() *HTS221 {
	err := i2c.Setregister(s.conn, 0x22, 0x00, 0b1111_1011)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *HTS221) Temperature() (float64, error) {
	if s == nil {
		return 0, fmt.Errorf("receiver is nil")
	}
	var t0_out_u uint16
	_, err := i2c.ReadTwoBytes(s.conn, 0x3c, &t0_out_u)
	if err != nil {
		return 0, err
	}
	t0_out := int16(t0_out_u)
	var t1_out_u uint16
	_, err = i2c.ReadTwoBytes(s.conn, 0x3e, &t1_out_u)
	if err != nil {
		return 0, err
	}
	t1_out := int16(t1_out_u)
	var t0_degc_x8_8 uint8
	_, err = i2c.ReadOneByte(s.conn, 0x32, &t0_degc_x8_8)
	if err != nil {
		return 0, err
	}
	var t1_degc_x8_8 uint8
	_, err = i2c.ReadOneByte(s.conn, 0x33, &t1_degc_x8_8)
	if err != nil {
		return 0, err
	}
	var t0t1_msb uint8
	_, err = i2c.ReadOneByte(s.conn, 0x35, &t0t1_msb)
	if err != nil {
		return 0, err
	}
	var t0_degc_x8 uint16 = uint16(t0t1_msb)
	t0_degc_x8 &= 0x0c
	t0_degc_x8 <<= 6
	t0_degc_x8 |= uint16(t0_degc_x8_8)
	var t1_degc_x8 uint16 = uint16(t0t1_msb)
	t1_degc_x8 &= 0x03
	t1_degc_x8 <<= 8
	t1_degc_x8 |= uint16(t1_degc_x8_8)
	var t_out_u uint16
	_, err = i2c.ReadTwoBytes(s.conn, 0x2a, &t_out_u)
	if err != nil {
		return 0, err
	}
	t_out := int16(t_out_u)
	first_numerator := t1_out - t0_out
	first_denominator := t1_out - t0_out
	first := float64(first_numerator) / float64(first_denominator)

	second_numerator := t_out - t0_out
	second_denominator := t1_out - t0_out
	second := float64(second_numerator) / float64(second_denominator)

	out := (float64(t0_degc_x8) * first) + (float64(t1_degc_x8) * second)
	return out / 8, nil
}

func (s *HTS221) Humidity() (float64, error) {
	if s == nil {
		return 0, fmt.Errorf("receiver is nil")
	}
	var h0_t0_out_u uint16
	_, err := i2c.ReadTwoBytes(s.conn, 0x36, &h0_t0_out_u)
	if err != nil {
		return 0, err
	}
	h0_t0_out := int16(h0_t0_out_u)
	var h1_t0_out_u uint16
	_, err = i2c.ReadTwoBytes(s.conn, 0x3a, &h1_t0_out_u)
	if err != nil {
		return 0, err
	}
	h1_t0_out := int16(h1_t0_out_u)
	var h0_rh_x2 uint8
	_, err = i2c.ReadOneByte(s.conn, 0x30, &h0_rh_x2)
	if err != nil {
		return 0, err
	}
	var h1_rh_x2 uint8
	_, err = i2c.ReadOneByte(s.conn, 0x31, &h1_rh_x2)
	if err != nil {
		return 0, err
	}
	var h_out_u uint16
	_, err = i2c.ReadTwoBytes(s.conn, 0x28, &h_out_u)
	if err != nil {
		return 0, err
	}
	h_out := int16(h_out_u)
	first_numerator := h1_t0_out - h_out
	first_denominator := h1_t0_out - h0_t0_out
	first := float64(first_numerator) / float64(first_denominator)

	second_numerator := h_out - h0_t0_out
	second_denominator := h1_t0_out - h0_t0_out
	second := float64(second_numerator) / float64(second_denominator)

	out := (float64(h0_rh_x2) * first) + (float64(h1_rh_x2) * second)
	return out / 2, nil
}
