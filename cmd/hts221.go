package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/unisense/unisense/sensors/hts221"
)

func init() {
	rootCmd.AddCommand(hts221Cmd)
}

var hts221Cmd = &cobra.Command{
	Use:   "hts221",
	Short: "hts221 temperature and humidity sensor",
	Run: func(cmd *cobra.Command, args []string) {
		s := hts221.New("/dev/i2c-1").
			WithContinousUpdate().
			DisableHeater().
			WithDataRate7Hz().
			WithDataReadyDisabled().
			WithHumiditySampleAveraging(hts221.HumiditySampleAveraging16).
			WithTemperatureSampleAveraging(hts221.TemperatureSampleAveraging16)
			temp, err := s.Temperature()
			if err != nil {
				panic(err)
			}
			fmt.Printf("temperature: %.3f °C\n", temp)
			hum, err := s.Humidity()
			if err != nil {
				panic(err)
			}
			fmt.Printf("humidity: %.3f\n", hum)
	},
}
